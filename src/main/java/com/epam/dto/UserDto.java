package com.epam.dto;

import java.util.List;

public class UserDto {

	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String username) {
		this.firstName = username;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String password) {
		this.lastName = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserDto(Long id, String username, String password, String email) {
		super();
		this.id = id;
		this.firstName = username;
		this.lastName = password;
		this.email = email;
	}

	public UserDto() {
		super();
	}

}
