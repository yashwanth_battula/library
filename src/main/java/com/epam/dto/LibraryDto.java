package com.epam.dto;

import com.epam.entity.Library;

public class LibraryDto {

	private Long id;
	private Long userId;
	private Long bookId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getBookId() {
		return bookId;
	}
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	@Override
	public String toString() {
		return "LibraryDto [id=" + id + ", userId=" + userId + ", bookId=" + bookId + "]";
	}
	public LibraryDto(Long id, Long userId, Long bookId) {
		super();
		this.id = id;
		this.userId = userId;
		this.bookId = bookId;
	}
	public LibraryDto() {
		super();
	}
	
	public LibraryDto(Library library) {
		this.id = library.getId();
		this.bookId = library.getBookId();
		this.userId = library.getUserId();
	}
	
	
}
