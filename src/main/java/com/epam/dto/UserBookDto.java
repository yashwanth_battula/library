package com.epam.dto;

import java.util.List;

public class UserBookDto {
	
	private UserDto user;
	private List<BookDTO> bookList;
	public UserDto getUser() {
		return user;
	}
	public void setUser(UserDto user) {
		this.user = user;
	}
	public List<BookDTO> getBookList() {
		return bookList;
	}
	public void setBookList(List<BookDTO> bookList) {
		this.bookList = bookList;
	}
	public UserBookDto() {
		super();
	}
}
