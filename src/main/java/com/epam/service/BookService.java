package com.epam.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.epam.client.BookServiceClient;
import com.epam.dto.BookDTO;
import com.epam.entity.Library;
import com.epam.repository.LibraryRepository;

@Service
public class BookService {
	
	private Logger logger = LogManager.getLogger(BookService.class);

	@Autowired
	BookServiceClient bookClient;
	
	@Autowired
	LibraryRepository libraryRepository;
	

	public ResponseEntity<List<BookDTO>> getAllBooks() {
		logger.info("get books");
		return bookClient.findAllBooks();
	}


	public ResponseEntity<BookDTO> getBookById(Long id) {
		logger.info("get book by BookId :{}",id);
		return bookClient.findBookById(id);
	}


	public ResponseEntity<BookDTO> addNewBook(BookDTO bookDTO) {
		logger.info("add new Book");
		return bookClient.addNewBook(bookDTO);
	}


	public ResponseEntity<?> deleteBookById(Long id) {
		logger.info("delete book by bookId :{}",id);
		List<Library> libraryList = libraryRepository.findByBookId(id);
		libraryRepository.deleteInBatch(libraryList);
		return bookClient.deleteBookById(id);
	}


	public ResponseEntity<BookDTO> updateBook(Long id, BookDTO bookDTO) {
		logger.info("update book by bookId:{}",id);
		return bookClient.updateBook(id, bookDTO);
	}
	
}
