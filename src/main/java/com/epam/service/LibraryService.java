package com.epam.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.BookDTO;
import com.epam.dto.LibraryDto;
import com.epam.entity.Library;
import com.epam.exceptions.LibraryItemNotFoundException;
import com.epam.repository.LibraryRepository;

@Service
public class LibraryService {
	
	private Logger logger = LogManager.getLogger(LibraryService.class);
	
	@Autowired
	LibraryRepository libraryRepository;
	
	@Autowired
	BookService bookService;

	public List<BookDTO> getBooksByUserid(Long userId){
		logger.info("get books related to userId :{}",userId);
		List<BookDTO> booksList = new ArrayList<>();
		List<Library> libraryList = libraryRepository.findByUserId(userId);
		for(Library library : libraryList) {
			booksList.add(bookService.getBookById(library.getBookId()).getBody());
		}
		return booksList;
	}

	public LibraryDto addBookToUser(Long userId , Long bookId){
		logger.info("add book to User with userId:{}",userId);
		Library libraryToBeAdded = new Library();
		libraryToBeAdded.setBookId(bookId);
		libraryToBeAdded.setUserId(userId);
		libraryRepository.save(libraryToBeAdded);
		return new LibraryDto(libraryToBeAdded);
	}

	public LibraryDto deleteBookForUser(Long userId, Long bookId) throws LibraryItemNotFoundException {
		logger.info("delete book related to userId :{}",userId);
		List<Library> libraryList = libraryRepository.findByUserId(userId);
		if(libraryList.isEmpty()) {
			logger.error("Exception occured while fetching books. Books not found for the user");
			throw new LibraryItemNotFoundException("Books not found under the user");
		}
		Library libraryToBeDeleted = new Library();
		for(Library library : libraryList) {
			if(library.getBookId() == bookId) {
				libraryToBeDeleted = library ; 
			}
		}
		libraryRepository.delete(libraryToBeDeleted);
		return new LibraryDto(libraryToBeDeleted);
	}

}
