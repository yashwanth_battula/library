package com.epam.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.epam.client.UserServiceClient;
import com.epam.dto.UserDto;
import com.epam.entity.Library;
import com.epam.repository.LibraryRepository;

@Service
public class UserService {
	
	private Logger logger = LogManager.getLogger(UserService.class);
	
	@Autowired
	UserServiceClient userClient;
	
	@Autowired
	LibraryRepository libraryRepository;

	public ResponseEntity<List<UserDto>> findAllUsers() {
		logger.info("get all users");
		return userClient.findAllUsers();
	}

	public ResponseEntity<UserDto> findUserById(Long id) {
		logger.info("get user by userId:{}",id);
		return userClient.findUserById(id);
	}

	public ResponseEntity<UserDto> addNewUser(UserDto userDto) {
		logger.info("add new user");
		return userClient.addNewUser(userDto);
	}

	public ResponseEntity<?> deleteUserById(Long id) {
		logger.info("delete user");
		List<Library> libraryList = libraryRepository.findByUserId(id);
		libraryRepository.deleteInBatch(libraryList);
		return userClient.deleteUserById(id);
	}

	public ResponseEntity<UserDto> updateUser(Long id, UserDto userDto) {
		logger.info("update user");
		return userClient.updateUser(id,userDto);
	}
	
	

}
