package com.epam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.entity.Library;

public interface LibraryRepository extends JpaRepository<Library, Long>{

	List<Library> findByBookId(Long id);
	
	List<Library> findByUserId(Long id);

}
