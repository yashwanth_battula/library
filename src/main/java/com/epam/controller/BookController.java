package com.epam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.epam.dto.BookDTO;
import com.epam.service.BookService;

import feign.FeignException.FeignClientException;

@RestController
@RequestMapping("/library")
@CrossOrigin(origins = "http://localhost:4200")
public class BookController {
	
	private Logger logger = LogManager.getLogger(BookController.class);

	@Autowired
	BookService bookService;
	
	@GetMapping("/books")
	public ResponseEntity<List<BookDTO>> getAllBooks(){
		logger.info("get all books");
		ResponseEntity<List<BookDTO>> bookListResponse;
		try {
			bookListResponse = bookService.getAllBooks();
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at fetching books",e);
			bookListResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return bookListResponse;
	}
	
	@GetMapping("/books/{book_id}")
	public ResponseEntity<BookDTO> getBookById(@PathVariable(value = "book_id") Long bookId){
		logger.info("get book");
		ResponseEntity<BookDTO> book ;
		try {
			book = bookService.getBookById(bookId);
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at fetching book",e);
			book = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return book;
	}
	
	@PostMapping("/books")
	public ResponseEntity<BookDTO> addNewBook(@RequestBody BookDTO bookDTO){
		logger.info("add new book");
		return bookService.addNewBook(bookDTO);
	}
	
	@DeleteMapping("/books/{book_id}")
	public ResponseEntity<?> deleteBookById(@PathVariable(value = "book_id") Long bookId){
		logger.info("delete book");
		ResponseEntity<?> response;
		try {
			response = bookService.deleteBookById(bookId);
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at delete book",e);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}
	
	@PutMapping("/books/{bookId}")
	public ResponseEntity<BookDTO> updateBook(@PathVariable(value = "bookId") Long bookId,@RequestBody BookDTO bookDTO){
		logger.info("update book");
		ResponseEntity<BookDTO> book;
		try {
			book = bookService.updateBook(bookId,bookDTO);
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at update book",e);
			book = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return book;
	}
	
}
