package com.epam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.BookDTO;
import com.epam.dto.LibraryDto;
import com.epam.dto.UserBookDto;
import com.epam.dto.UserDto;
import com.epam.exceptions.LibraryItemNotFoundException;
import com.epam.service.LibraryService;
import com.epam.service.UserService;

import feign.FeignException.FeignClientException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class LibraryController {
	
	private Logger logger = LogManager.getLogger(LibraryController.class);

	@Autowired
	LibraryService libraryService;
	
	@Autowired
	UserService userService;
	
	@GetMapping("/library/{user_id}")
	public ResponseEntity<UserDto> findUserById(@PathVariable(value = "user_id") Long userId){
		logger.info("get user");
		ResponseEntity<UserDto> user;
		try {
			user = userService.findUserById(userId);
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at fetching user",e);
			user = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return user;
	}
	
	@GetMapping("/library/users/{user_id}")
	public ResponseEntity<UserBookDto> getUserWithBooks(@PathVariable (value = "user_id") Long userId){
		logger.info("get user information and books related to user");
		ResponseEntity<UserBookDto> userBookResponse;
		try {
			UserDto user = userService.findUserById(userId).getBody();
			List<BookDTO> bookList = libraryService.getBooksByUserid(userId);
			UserBookDto userBook = new UserBookDto();
			userBook.setUser(user);
			userBook.setBookList(bookList);
			userBookResponse = ResponseEntity.ok(userBook);
		}catch(FeignClientException | LibraryItemNotFoundException e) {
			logger.error("Exception {} occured at fetching user information and books",e);
			userBookResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return userBookResponse;
	}
	
	@PostMapping("/library/users/{user_id}/books/{book_id}")
	public ResponseEntity<LibraryDto> addBookToUser(@PathVariable (value = "user_id") Long userId , @PathVariable (value = "book_id") Long bookId){
		logger.info("add a new book to user");
		return ResponseEntity.ok(libraryService.addBookToUser(userId , bookId));
	}
	
	@DeleteMapping("/library/users/{user_id}/books/{book_id}")
	public ResponseEntity<LibraryDto> deleteBookForUser(@PathVariable (value = "user_id") Long userId , @PathVariable (value = "book_id") Long bookId){
		logger.info("delete a book for the user");
		ResponseEntity<LibraryDto> libraryDto ;
		try {
			libraryDto = ResponseEntity.ok(libraryService.deleteBookForUser(userId , bookId));
		}catch(LibraryItemNotFoundException e) {
			logger.error("Exception {} occured at delete books under the userId {}",e,userId);
			libraryDto = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return libraryDto;
	}
	
}
