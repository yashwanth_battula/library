package com.epam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.UserDto;
import com.epam.service.UserService;

import feign.FeignException.FeignClientException;

@RestController
@RequestMapping("/library")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	
	private Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@GetMapping("/users")
	public ResponseEntity<List<UserDto>> findAllUsers(){
		logger.info("get users");
		ResponseEntity<List<UserDto>> userList;
		try {
			userList = userService.findAllUsers();
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at fetching users",e);
			userList = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return userList;
	}
	
	@PostMapping("/users")
	public ResponseEntity<UserDto> addNewUser(@RequestBody UserDto userDto){
		logger.info("add new user");
		return userService.addNewUser(userDto);
	}
	
	@DeleteMapping("/users/{user_id}")
	public ResponseEntity<?> deleteUserById(@PathVariable(value = "user_id") Long userId){
		logger.info("delete user");
		ResponseEntity<?> user;
		try {
			user = userService.deleteUserById(userId);
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at delete user",e);
			user = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return user;
	}
		
	@PutMapping("/users/{user_id}")
	public ResponseEntity<UserDto> updateUser(@PathVariable(value = "user_id") Long userId, @RequestBody UserDto userDto){
		logger.info("update user");
		ResponseEntity<UserDto> user;
		try {
			user = userService.updateUser(userId , userDto);
		}catch(FeignClientException e) {
			logger.error("Exception {} occured at update user",e);
			user = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return user;
	}
	

}
