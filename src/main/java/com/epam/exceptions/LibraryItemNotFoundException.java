package com.epam.exceptions;

public class LibraryItemNotFoundException extends RuntimeException{
	
	public LibraryItemNotFoundException(String errorMsg){
		super(errorMsg);
	}
	
}
