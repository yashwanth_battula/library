package com.epam.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.client.fallback.UserClientFallBack;
import com.epam.dto.UserDto;


@Component
@FeignClient(name = "userservice" , fallback = UserClientFallBack.class)
public interface UserServiceClient {
	
	@GetMapping("/user")
	ResponseEntity<List<UserDto>> findAllUsers();
	
	@GetMapping("/user/{UserId}")
    ResponseEntity<UserDto> findUserById(@PathVariable(value = "UserId") long userId);

	@PostMapping("/user")
	ResponseEntity<UserDto> addNewUser(@RequestBody UserDto userDto);
	
	@DeleteMapping("/user/{UserId}")
	ResponseEntity<?> deleteUserById(@PathVariable(value = "UserId") Long userId);
		
	@PutMapping("/user/{UserId}")
	ResponseEntity<UserDto> updateUser(@PathVariable(value = "UserId") Long userId, @RequestBody UserDto userDto);
	
}
