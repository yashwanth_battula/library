package com.epam.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.client.fallback.BookClientFallBack;
import com.epam.dto.BookDTO;


@FeignClient(name = "bookservice" , fallback = BookClientFallBack.class)
@Component
public interface BookServiceClient {

	@GetMapping("/book")
	ResponseEntity<List<BookDTO>> findAllBooks();

	@GetMapping("/book/{bookId}")
	ResponseEntity<BookDTO> findBookById(@PathVariable("bookId") Long bookId);
	
	@PostMapping("/book")
	ResponseEntity<BookDTO> addNewBook(@RequestBody BookDTO book);
	
	@DeleteMapping("book/{bookId}")
	ResponseEntity<?> deleteBookById(@PathVariable("bookId") Long bookId);
	
	@PutMapping("book/{bookId}")
	ResponseEntity<BookDTO> updateBook(@PathVariable("bookId") Long bookId, @RequestBody BookDTO bookDTO);

}
