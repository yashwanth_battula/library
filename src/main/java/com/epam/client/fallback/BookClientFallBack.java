package com.epam.client.fallback;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.epam.client.BookServiceClient;
import com.epam.dto.BookDTO;

@Component
public class BookClientFallBack implements BookServiceClient{

	@Override
	public ResponseEntity<List<BookDTO>> findAllBooks() {
		List<BookDTO> booklist = new ArrayList<>();
		booklist.add(new BookDTO(1L,"ab","ab","ab",1));
		return ResponseEntity.ok(booklist);
	}

	@Override
	public ResponseEntity<BookDTO> findBookById(Long id) {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		return ResponseEntity.ok(book);
	}

	@Override
	public ResponseEntity<BookDTO> addNewBook(BookDTO book) {
		return getServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<?> deleteBookById(Long id) {
		return getServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<BookDTO> updateBook(Long id, BookDTO bookDTO) {
		return getServiceUnavailableResponseEntity();
	}
	
	public static ResponseEntity<BookDTO> getServiceUnavailableResponseEntity(){
        return new ResponseEntity<BookDTO>(HttpStatus.SERVICE_UNAVAILABLE);
    }

}
