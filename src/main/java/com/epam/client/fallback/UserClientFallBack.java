package com.epam.client.fallback;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.epam.client.UserServiceClient;
import com.epam.dto.UserDto;

@Component
public class UserClientFallBack implements UserServiceClient{

	@Override
	public ResponseEntity<List<UserDto>> findAllUsers() {
		List<UserDto> userList = new ArrayList<>();
		userList.add(new UserDto(1L, "username" , "password" , "email"));
		return ResponseEntity.ok(userList);
	}

	@Override
	public ResponseEntity<UserDto> findUserById(long id) {
		return ResponseEntity.ok(new UserDto(1L, "username" , "password" , "email"));
	}

	@Override
	public ResponseEntity<UserDto> addNewUser(UserDto userDto) {
		return getServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<?> deleteUserById(Long id) {
		return getServiceUnavailableResponseEntity();
	}

	@Override
	public ResponseEntity<UserDto> updateUser(Long id, UserDto userDto) {
		return getServiceUnavailableResponseEntity();
	}

	public static ResponseEntity<UserDto> getServiceUnavailableResponseEntity(){
        return new ResponseEntity<UserDto>(HttpStatus.SERVICE_UNAVAILABLE);
    }
	
}
