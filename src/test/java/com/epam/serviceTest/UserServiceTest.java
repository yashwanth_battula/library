package com.epam.serviceTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.epam.client.UserServiceClient;
import com.epam.dto.UserDto;
import com.epam.repository.LibraryRepository;
import com.epam.service.UserService;

public class UserServiceTest {
	@Mock
	UserServiceClient userClient;
	
	@Mock
	LibraryRepository libraryRepository;
	
	@InjectMocks
	UserService userService;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAllUsers() {
		List<UserDto> users = new ArrayList<>();
		users.add(new UserDto(1L, "abc", "abc", "abc"));
		ResponseEntity<List<UserDto>> response = new ResponseEntity<>(users , HttpStatus.OK);
		when(userClient.findAllUsers()).thenReturn(response);
		assertEquals(userService.findAllUsers().getBody() , response.getBody());
	}
	
	@Test
	public void testFindByUserId() {
		UserDto user = new UserDto(1L, "abc", "abc", "abc");
		ResponseEntity<UserDto> response = new ResponseEntity<>(user , HttpStatus.OK);
		when(userClient.findUserById(1L)).thenReturn(response);
		assertEquals(userService.findUserById(1L).getBody() , response.getBody());
	}
	
	@Test
	public void testAddNewUser() {
		UserDto user = new UserDto(1L, "abc", "abc", "abc");
		ResponseEntity<UserDto> response = new ResponseEntity<>(user , HttpStatus.OK);
		when(userClient.addNewUser(user)).thenReturn(response);
		assertEquals(userService.addNewUser(user).getBody() , response.getBody());
	}
	
	@Test
	public void testUpdateUser() {
		UserDto user = new UserDto(1L, "abc", "abc", "abc");
		ResponseEntity<UserDto> response = new ResponseEntity<>(user , HttpStatus.OK);
		when(userClient.updateUser(1L,user)).thenReturn(response);
		assertEquals(userService.updateUser(1L,user).getBody() , response.getBody());
	}
	

}
