package com.epam.serviceTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.epam.client.BookServiceClient;
import com.epam.dto.BookDTO;
import com.epam.repository.LibraryRepository;
import com.epam.service.BookService;

public class BookServiceTest {
	@Mock
	BookServiceClient bookClient;
	
	@Mock
	LibraryRepository libraryRepository;
	
	@InjectMocks
	BookService bookService;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAllBooks() {
		List<BookDTO> booksList = new ArrayList<>();
		booksList.add(new BookDTO(1L,"ab","ab","ab",1));
		ResponseEntity<List<BookDTO>> responseEntity = new ResponseEntity<>(booksList , HttpStatus.OK);
		when(bookClient.findAllBooks()).thenReturn(responseEntity);	
		assertEquals(bookService.getAllBooks().getBody() , responseEntity.getBody());
	}
	
	@Test
	public void testFindBookById() {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		ResponseEntity<BookDTO> responseEntity = new ResponseEntity<>(book , HttpStatus.OK);
		when(bookClient.findBookById(1L)).thenReturn(responseEntity);	
		assertEquals(bookService.getBookById(1L).getBody() , responseEntity.getBody());
	}
	
	@Test
	public void testAddBook() {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		ResponseEntity<BookDTO> responseEntity = new ResponseEntity<>(book , HttpStatus.OK);
		when(bookClient.addNewBook(book)).thenReturn(responseEntity);	
		assertEquals(bookService.addNewBook(book).getBody() , responseEntity.getBody());
	}
	
	@Test
	public void testUpdateBook() {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		ResponseEntity<BookDTO> responseEntity = new ResponseEntity<>(book , HttpStatus.OK);
		when(bookClient.updateBook(1L, book)).thenReturn(responseEntity);	
		assertEquals(bookService.updateBook(1L, book).getBody() , responseEntity.getBody());
	}
		
	
}
