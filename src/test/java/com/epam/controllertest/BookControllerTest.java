package com.epam.controllertest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.epam.controller.BookController;
import com.epam.dto.BookDTO;
import com.epam.service.BookService;

import feign.FeignException.FeignClientException;

public class BookControllerTest {
	
	@Mock
	BookService bookService;
	
	@InjectMocks
	BookController bookController;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetAllBooks() {
		List<BookDTO> booksList = new ArrayList<>();
		booksList.add(new BookDTO(1L,"ab","ab","ab",1));
		ResponseEntity<List<BookDTO>> response = new ResponseEntity<>(booksList , HttpStatus.OK);
		when(bookService.getAllBooks()).thenReturn(response);
		assertEquals(bookController.getAllBooks().getBody() , response.getBody());
	}
	
	@Test
	public void testGetAllBooksIfNotFound() {
		when(bookService.getAllBooks()).thenThrow(FeignClientException.class);
		assertEquals(bookController.getAllBooks().getStatusCode() , HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void testGetBookById() {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		ResponseEntity<BookDTO> response = new ResponseEntity<>(book , HttpStatus.OK);
		when(bookService.getBookById(1L)).thenReturn(response);
		assertEquals(bookController.getBookById(1L).getBody() , response.getBody());
	}
	
	@Test
	public void testGetBookIfNotFound() {
		when(bookService.getBookById(1L)).thenThrow(FeignClientException.class);
		assertEquals(bookController.getBookById(1L).getStatusCode() , HttpStatus.NOT_FOUND);
	}
	
	
	@Test
	public void testAddNewBook() {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		ResponseEntity<BookDTO> response = new ResponseEntity<>(book , HttpStatus.OK);
		when(bookService.addNewBook(book)).thenReturn(response);
		assertEquals(bookController.addNewBook(book).getBody() , response.getBody());
	}
	
	@Test
	public void testUpdateBook() {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		ResponseEntity<BookDTO> response = new ResponseEntity<>(book , HttpStatus.OK);
		when(bookService.updateBook(1L, book)).thenReturn(response);
		assertEquals(bookController.updateBook(1L , book).getBody() , response.getBody());
	}
	
	@Test
	public void testUpdateBookIfNotFound() {
		BookDTO book = new BookDTO(1L,"ab","ab","ab",1);
		when(bookService.updateBook(1L, book)).thenThrow(FeignClientException.class);
		assertEquals(bookController.updateBook(1L , book).getStatusCode() , HttpStatus.NOT_FOUND);
	}
	
}
