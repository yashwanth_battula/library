package com.epam.controllertest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.epam.controller.UserController;
import com.epam.dto.UserDto;
import com.epam.service.UserService;

import feign.FeignException.FeignClientException;

public class UserControllerTest {
	
	@Mock
	UserService userService;
	
	@InjectMocks
	UserController userController;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetAllUsers() {
		List<UserDto> usersList = new ArrayList<>();
		usersList.add(new UserDto(1L,"ab","ab","ab"));
		ResponseEntity<List<UserDto>> response = new ResponseEntity<>(usersList , HttpStatus.OK);
		when(userService.findAllUsers()).thenReturn(response);
		assertEquals(userController.findAllUsers().getBody() , response.getBody());
	}
	
	@Test
	public void testGetAllUsersIfNotFound() {
		when(userService.findAllUsers()).thenThrow(FeignClientException.class);
		assertEquals(userController.findAllUsers().getStatusCode() , HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void testAddNewUser() {
		UserDto user = new UserDto(1L,"ab","ab","ab");
		ResponseEntity<UserDto> response = new ResponseEntity<>(user , HttpStatus.OK);
		when(userService.addNewUser(user)).thenReturn(response);
		assertEquals(userController.addNewUser(user).getBody() , response.getBody());
	}
		
	@Test
	public void testUpdateUser() {
		UserDto user = new UserDto(1L,"ab","ab","ab");
		ResponseEntity<UserDto> response = new ResponseEntity<>(user , HttpStatus.OK);
		when(userService.updateUser(1L , user)).thenReturn(response);
		assertEquals(userController.updateUser(1L , user).getBody() , response.getBody());
	}
	
	@Test
	public void testUpdateUserIfNotFound() {
		UserDto user = new UserDto(1L,"ab","ab","ab");
		when(userService.updateUser(1L , user)).thenThrow(FeignClientException.class);
		assertEquals(userController.updateUser(1L , user).getStatusCode() , HttpStatus.NOT_FOUND);
	}
	

}
